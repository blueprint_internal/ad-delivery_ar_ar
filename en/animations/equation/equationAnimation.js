
function EquationAnimation(resources)
{
	EquationAnimation.resources = resources;
}
EquationAnimation.prototype = {
	init: function()
	{
		this.game = new Phaser.Game(800, 400, Phaser.CANVAS, 'EquationAnimation', { preload: this.preload, create: this.create, update: this.update, render: 
		this.render,parent:this });
	},

	preload: function()
	{
		this.game.scale.maxWidth = 800;
    	this.game.scale.maxHeight = 400;
		this.game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
		
    	this.game.load.image('yellow', EquationAnimation.resources.yellow);
    	this.game.load.image('redplus', EquationAnimation.resources.redplus);
    	this.game.load.image('equals', EquationAnimation.resources.equals);
    	this.game.load.image('blue', EquationAnimation.resources.blue);
    	this.game.load.image('green', EquationAnimation.resources.green);
    	this.game.stage.backgroundColor = '#ffffff'
    	
	},

	create: function(evt)
	{
		this.game.stage.backgroundColor = '#ffffff'
		this.parent.yellow = this.game.add.sprite(this.game.world.centerX,this.game.world.centerY, 'yellow');
		this.parent.yellow.alpha = 1; 
		this.parent.redplus = this.game.add.sprite(this.game.world.centerX,this.game.world.centerY, 'redplus');
		this.parent.redplus.alpha = 1;
		this.parent.green = this.game.add.sprite(this.game.world.centerX,this.game.world.centerY, 'green');
		this.parent.green.alpha = 1;  
		this.parent.equals = this.game.add.sprite(this.game.world.centerX,this.game.world.centerY, 'equals');
		this.parent.equals.alpha = 1; 
		this.parent.blue = this.game.add.sprite(this.game.world.centerX,this.game.world.centerY, 'blue');
		this.parent.blue.alpha = 1; 
		var style = { font: "bold 19px freight-sans-pro", fill: "#000000", wordWrap: true, wordWrapWidth:150, align: "center",lineSpacing: -10 };
		var style2 = { font: "bold 25px freight-sans-pro", fill: "#000000", wordWrap: true, wordWrapWidth:150, align: "center",lineSpacing: -10 };
		this.parent.leftText = this.game.add.text(this.game.world.centerX,this.game.world.centerY, EquationAnimation.resources.leftText, style);
		this.parent.centerText = this.game.add.text(this.game.world.centerX,this.game.world.centerY, EquationAnimation.resources.centerText, style);
		this.parent.rightText = this.game.add.text(this.game.world.centerX,this.game.world.centerY, EquationAnimation.resources.rightText, style2);
		this.parent.buildAnimation();
	},

	buildAnimation: function()
	{
		this.blue.anchor.set(0.5);
		this.equals.anchor.set(0.5);
		this.green.anchor.set(0.5);
		this.redplus.anchor.set(0.5);
		this.yellow.anchor.set(0.5);
		this.leftText.anchor.set(0.5);
		this.centerText.anchor.set(0.5);
		this.rightText.anchor.set(0.5);
		//
		
		this.blue.x = 800;
		this.rightText.x = 648;
		this.rightText.y = this.blue.y;
		this.rightText.alpha = 0;
		this.blue.alpha = 0;
		this.equals.x = 500;
		this.equals.alpha = 0;
		this.equals.rotation = 5;
		this.green.x = 380;
		this.green.alpha = 0;
		this.green.y = 100
		this.centerText.alpha = 0;
		this.centerText.y = this.game.world.centerY-70;
		this.centerText.x = 380;
		this.redplus.x = 250;
		this.redplus.alpha = 0;
		this.yellow.x = 0;
		this.yellow.alpha = 0;
		this.leftText.alpha = 0;
		this.leftText.x = 125;
		this.leftText.y = this.game.world.centerY-20;

		this.redplus.y = 260;
		this.yellowAn = this.game.add.tween(this.yellow).to( { alpha:1,x: 125 },800,Phaser.Easing.Quadratic.Out);
		this.leftTextAn = this.game.add.tween(this.leftText).to( { alpha:1,y:this.game.world.centerY },300,Phaser.Easing.Quadratic.Out);
		this.redPlusAn = this.game.add.tween(this.redplus).to( { alpha:1,y:this.game.world.centerY},600,Phaser.Easing.Quadratic.Out);
		this.greenAn = this.game.add.tween(this.green).to( { alpha:1,y:this.game.world.centerY},800,Phaser.Easing.Quadratic.Out);
		this.centerTextAn = this.game.add.tween(this.centerText).to( { alpha:1,y:this.game.world.centerY-70 },400,Phaser.Easing.Quadratic.Out);
		this.rightTextAn = this.game.add.tween(this.rightText).to( {alpha:1},400,Phaser.Easing.Quadratic.Out);
		this.equalsAn = this.game.add.tween(this.equals).to( { alpha:1,rotation:0},600,Phaser.Easing.Quadratic.Out);
		this.blueAn = this.game.add.tween(this.blue).to( { alpha:1,x:648},800,Phaser.Easing.Quadratic.Out);
		this.yellowAn.chain (this.leftTextAn,this.redPlusAn,this.greenAn,this.centerTextAn,this.equalsAn,this.blueAn,this.rightTextAn);
		this.yellowAn.start();
		/*
		this.lines.y = 210;
		//this.trophy.y = 290;
		this.paper.y = 100;
		this.money.x = 270;
		this.money.y = 255;
		this.star.x = 520;
		this.star.y = 250;

		this.topText.y = 40;
		this.footerText.y = 380;
		this.leftText.x = 272;
		this.leftText.y = 180;
		this.rightText.x = 520;
		this.rightText.y = 180;

		this.trophyAnimation = this.game.add.tween(this.trophy).to( { alpha:1,y: 290 },1000,Phaser.Easing.Bounce.Out);
		this.linesAnimation = this.game.add.tween(this.lines).to( { alpha:1},1000);
		this.moneyAnimation = this.game.add.tween(this.money).to( { alpha:1,y:255},1000,Phaser.Easing.Bounce.Out);
		this.starAnimation = this.game.add.tween(this.star).to( { alpha:1,y:255},1000,Phaser.Easing.Bounce.Out);
		this.paperAnimation = this.game.add.tween(this.paper).to( { alpha:1,y:100},1000,Phaser.Easing.Bounce.Out);
		//
		this.leftTextAn = this.game.add.tween(this.leftText).to( { alpha:1},500);
		this.topTextAn = this.game.add.tween(this.topText).to( { alpha:1},500);
		this.rightTextAn = this.game.add.tween(this.rightText).to( { alpha:1},500);
		this.footerTextAn = this.game.add.tween(this.footerText).to( { alpha:1},500);




		this.trophyAnimation.chain(this.linesAnimation,this.moneyAnimation,this.paperAnimation,this.starAnimation,this.leftTextAn,this.topTextAn,this.rightTextAn,this.footerTextAn);
		
		this.animate();
		
	},

	animate: function()
	{

		this.blue.anchor.set(0.5);
		/*
		this.background.anchor.set(0.5);
		this.lines.anchor.set(0.5);
		this.lines.alpha = 0;
		this.star.anchor.set(0.5);
		this.star.alpha = 0;
		this.star.y -=40;
		this.money.anchor.set(0.5);
		this.paper.anchor.set(0.5);
		this.paper.alpha = 0;
		this.paper.y-=40
		this.trophy.alpha = 0;
		this.trophy.anchor.set(0.5);
		this.trophy.y+=40;

		this.money.y -=40;
		this.trophyAnimation.start();
		*/
	},

	update: function()
	{

	},

	render: function()
	{
		//this.game.debug.inputInfo(32, 32);
	}

}


