
function AdRanking(resources)
{
	AdRanking.resources = resources;
}
AdRanking.prototype = {
	init: function()
	{
		this.game = new Phaser.Game(800, 400, Phaser.CANVAS, 'AdRanking', { preload: this.preload, create: this.create, update: this.update, render: 
		this.render,parent:this });
	},

	preload: function()
	{
		this.game.scale.maxWidth = 800;
    	this.game.scale.maxHeight = 400;
		this.game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
		this.game.load.image('background', AdRanking.resources.background);
    	this.game.load.image('star', AdRanking.resources.star);
    	this.game.load.image('money', AdRanking.resources.money);
    	this.game.load.image('paper', AdRanking.resources.paper);
    	this.game.load.image('trophy', AdRanking.resources.trophy);
    	this.game.load.image('lines', AdRanking.resources.lines);
    	this.game.stage.backgroundColor = '#ffffff'
	},

	create: function(evt)
	{
		this.game.stage.backgroundColor = '#ffffff' 
		this.parent.background = this.game.add.sprite(this.game.world.centerX,this.game.world.centerY, 'background');
		this.parent.lines = this.game.add.sprite(this.game.world.centerX,this.game.world.centerY, 'lines');
		this.parent.lines.alpha = 0;
		this.parent.star = this.game.add.sprite(this.game.world.centerX,this.game.world.centerY, 'star');
		this.parent.star.alpha = 0;
		this.parent.money = this.game.add.sprite(this.game.world.centerX,this.game.world.centerY, 'money');
		this.parent.money.alpha =0;
		this.parent.paper = this.game.add.sprite(this.game.world.centerX,this.game.world.centerY, 'paper');
		this.parent.paper.alpha = 0;
		this.parent.trophy= this.game.add.sprite(this.game.world.centerX,this.game.world.centerY, 'trophy');
		this.parent.trophy.alpha=0;
		
		var style = { font: "bold 19px freight-sans-pro", fill: "#000000", wordWrap: true, wordWrapWidth:150, align: "center",lineSpacing: -10 };
		var style2 = { font: "bold 19px freight-sans-pro", fill: "#8e0026", wordWrap: true, wordWrapWidth:300, align: "center",lineSpacing: -10 };
		this.parent.rightText = this.game.add.text(this.game.world.centerX,this.game.world.centerY, AdRanking.resources.rightText, style);
		this.parent.rightText.anchor.set(0.5);
		this.parent.rightText.alpha = 0;
		this.parent.leftText = this.game.add.text(this.game.world.centerX,this.game.world.centerY, AdRanking.resources.leftText, style);
		this.parent.leftText.anchor.set(0.5);
		this.parent.leftText.alpha = 0;
		this.parent.topText = this.game.add.text(this.game.world.centerX,this.game.world.centerY, AdRanking.resources.topText, style);
		this.parent.topText.alpha = 0;
		this.parent.topText.anchor.set(0.5);
		this.parent.footerText = this.game.add.text(this.game.world.centerX,this.game.world.centerY, AdRanking.resources.footerText, style2);
		this.parent.footerText.anchor.set(0.5);
		this.parent.footerText.alpha =0;
		
		
		this.parent.buildAnimation();

    	
		
	},

	buildAnimation: function()
	{
		this.lines.y = 210;
		//this.trophy.y = 290;
		this.paper.y = 100;
		this.money.x = 270;
		this.money.y = 255;
		this.star.x = 520;
		this.star.y = 250;

		this.topText.y = 40;
		this.footerText.y = 380;
		this.leftText.x = 272;
		this.leftText.y = 180;
		this.rightText.x = 520;
		this.rightText.y = 180;

		this.trophyAnimation = this.game.add.tween(this.trophy).to( { alpha:1,y: 290 },1000,Phaser.Easing.Bounce.Out);
		this.linesAnimation = this.game.add.tween(this.lines).to( { alpha:1},1000);
		this.moneyAnimation = this.game.add.tween(this.money).to( { alpha:1,y:255},1000,Phaser.Easing.Bounce.Out);
		this.starAnimation = this.game.add.tween(this.star).to( { alpha:1,y:255},1000,Phaser.Easing.Bounce.Out);
		this.paperAnimation = this.game.add.tween(this.paper).to( { alpha:1,y:100},1000,Phaser.Easing.Bounce.Out);
		//
		this.leftTextAn = this.game.add.tween(this.leftText).to( { alpha:1},500);
		this.topTextAn = this.game.add.tween(this.topText).to( { alpha:1},500);
		this.rightTextAn = this.game.add.tween(this.rightText).to( { alpha:1},500);
		this.footerTextAn = this.game.add.tween(this.footerText).to( { alpha:1},500);




		this.trophyAnimation.chain(this.linesAnimation,this.moneyAnimation,this.paperAnimation,this.starAnimation,this.leftTextAn,this.topTextAn,this.rightTextAn,this.footerTextAn);
		
		this.animate();
	},

	animate: function()
	{
		this.background.anchor.set(0.5);
		this.lines.anchor.set(0.5);
		this.lines.alpha = 0;
		this.star.anchor.set(0.5);
		this.star.alpha = 0;
		this.star.y -=40;
		this.money.anchor.set(0.5);
		this.paper.anchor.set(0.5);
		this.paper.alpha = 0;
		this.paper.y-=40
		this.trophy.alpha = 0;
		this.trophy.anchor.set(0.5);
		this.trophy.y+=40;

		this.money.y -=40;
		this.trophyAnimation.start();
	
	},

	update: function()
	{

	},

	render: function()
	{
		//this.game.debug.inputInfo(32, 32);
	}

}


